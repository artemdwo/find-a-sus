def find_sus(logs, threshold):

    records_list = logs_converter(logs)

    if len(records_list) == 1 and threshold > 0:
        sender_id, recipient_id, * = records_list[0]
        if threshold == 1 and sender_id == recipient_id:
            return [sender_id]
        else:
            return []
    elif len(records_list) > 1 and threshold > 0:
        transactions_count = {}
        for record in records_list:
            sender_id, recipient_id = record
            if sender_id == recipient_id:
                transactions_count[sender_id] += 1
        print(transactions_count)
        return [2]
    else:
        return []


""" Converts a list of strings into a list of lists

Returns list of lists
Returns empty list if no logs given
"""
def logs_converter(logs):
    records = []
    if not logs:
        return []
    else:
        for rec in logs:
            records.append(list(map(int, str.split(rec, ' '))))
        return records
