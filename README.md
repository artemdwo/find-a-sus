# Find a SUS

Identify suspicious activities from the logs and return a list of those IDs spotted to have at least a _threshold_ number of transactions in a given log

## Run tests
```python test_sus_finder.py```

or

```python3 test_sus_finder.py```