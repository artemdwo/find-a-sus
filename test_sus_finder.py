import unittest
import sus_finder


class TestMain(unittest.TestCase):

    def test_find_sus(self):
        # No logs
        self.assertEqual(sus_finder.find_sus([], 5), [])
        # Zero threshold
        self.assertEqual(sus_finder.find_sus(['1 2 10', '1 3 30'], 0), [])
        # Single log record + 1 threshold with no match
        self.assertEqual(sus_finder.find_sus(['1 2 10'], 1), [])
        # Single log record + 1 threshold with a match id 1
        self.assertEqual(sus_finder.find_sus(['1 1 10'], 1), [1])
        # Single log record + 1 threshold with a match id 99
        self.assertEqual(sus_finder.find_sus(['99 99 13220'], 1), [99])
        # Multiple log records
        self.assertEqual(sus_finder.find_sus(['1 2 10', '1 3 30'], 2), [2])
    
    def test_logs_converter(self):
        # No logs
        self.assertEqual(sus_finder.logs_converter([]), [])
        # Single log
        self.assertEqual(sus_finder.logs_converter(['1 2 10']), [[1, 2, 10]])
        # Some logs
        self.assertEqual(sus_finder.logs_converter(['1 2 10', '1 3 30']), [[1, 2, 10], [1, 3, 30]])
        # More logs
        self.assertEqual(sus_finder.logs_converter(['1 2 10', '1 3 30', '100 211 1033', '14 3 305']), [[1, 2, 10], [1, 3, 30], [100, 211, 1033], [14, 3, 305]])


if __name__ == '__main__':
    unittest.main()
